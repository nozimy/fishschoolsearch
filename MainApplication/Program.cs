﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Diagnostics;
using FishLibraryUniversal;


namespace MainApplication
{
    class Program
    {
         static UserFunctions.FuncTemplate selectedFunction;
        static int dimensionSize,iterationCount, populationSize;
        static double indivStepBegin, indivStepEnd, maxWeight;
        static FishPointUniversal beginArea, endArea;
        static FishUniversal[] fishes;
        static int iterationNumber;
        static BackgroundWorker bgWorker = new BackgroundWorker() { WorkerReportsProgress = true };
        static Stopwatch timer=new Stopwatch();
        
        
        static void Main(string[] args)
        {
            bgWorker.DoWork += (bg_sender, bg_e) =>
            {
                timer.Restart();
                for (iterationNumber = 1; iterationNumber <= iterationCount; iterationNumber++)
                {
                    bgWorker.ReportProgress((int)((float)iterationNumber / iterationCount * 100));
                    NextIteration();
                }
            };
            bgWorker.ProgressChanged += (progress_sender, progress_e) =>
            {
                Console.CursorLeft = 0;
                Console.Write("Процент выполнения алгоритма: {0}%", progress_e.ProgressPercentage);
            };
            bgWorker.RunWorkerCompleted += (completed_sender, completed_e) =>
            {
                timer.Stop();
                Console.WriteLine();
                Console.WriteLine("Максимальное значение функции: {0}", fishes.Max(fish => FishUniversal.Fitness(fish.swimStatePoint[3].GetCoords())));
                Console.WriteLine("Среднее значение функции: {0}", fishes.Sum(fish => FishUniversal.Fitness(fish.swimStatePoint[3].GetCoords())) / FishUniversal.populationSize);
                Console.WriteLine("Время выполнения алгоритма: {0:00}.{1:00} сек", timer.Elapsed.Seconds, timer.Elapsed.Milliseconds / 10);
                Console.WriteLine("Для выхода нажмите ESC!");
            };
            do
            {
                Console.Clear();
                UserFunctions.ShowFunctionList();
                InputData();
                bgWorker.RunWorkerAsync();
            }
            while (Console.ReadKey(true).Key != ConsoleKey.Escape);
            //Console.WriteLine(FishUniversal.Fitness(-0.44, -11)); 
        }
        static void InputData()
        {
            int selectedNumber;
            Tuple<UserFunctions.FuncTemplate,int> selectedItem;
            do
                Console.Write("Напишите номер выбранной функции: ");
            while (!int.TryParse(Console.ReadLine(), out selectedNumber) || null==(selectedItem=UserFunctions.SelectFunction(selectedNumber)));
            selectedFunction = selectedItem.Item1;
            dimensionSize = selectedItem.Item2;
            Console.WriteLine("------------------------------------");
            beginArea = new FishPointUniversal(dimensionSize);
            endArea = new FishPointUniversal(dimensionSize);
            Console.WriteLine("Ввод нижней границы области поиска:");
            //Console.WriteLine("------------------------------------");
            double varForOut=0;
            for (int i = 0; i < dimensionSize; i++)
            {
                do
                    Console.Write("Введите координату номер {0} (double):", i);
                while (!double.TryParse(Console.ReadLine(), out varForOut));
                beginArea[i] = varForOut;
            }
            Console.WriteLine("------------------------------------");
            Console.WriteLine("Ввод вверхней границы области поиска:");
            for (int i = 0; i < dimensionSize; i++)
            {
                do
                    Console.Write("Введите координату номер {0} (double, <Нижняя граница):", i);
                while (!double.TryParse(Console.ReadLine(), out varForOut) || varForOut<=beginArea[i]);
                endArea[i] = varForOut;
            }
            Console.WriteLine("------------------------------------");
            do
                Console.Write("Введите количество итераций (int, >0): ");
            while (!int.TryParse(Console.ReadLine(), out iterationCount) || iterationCount <= 0);
            Console.WriteLine("------------------------------------");
            do
                Console.Write("Введите размер популяции (int, >0): ");
            while (!int.TryParse(Console.ReadLine(), out populationSize) || populationSize <= 0);
            Console.WriteLine("------------------------------------");
            do
                Console.Write("Введите начальный индивидульаный шаг (double, >0): ");
            while (!double.TryParse(Console.ReadLine(), out indivStepBegin) || indivStepBegin <= 0);
            Console.WriteLine("------------------------------------");
            do
                Console.Write("Введите конечный индивидульаный шаг (double, >0, <Начальный шаг): ");
            while (!double.TryParse(Console.ReadLine(), out indivStepEnd) || indivStepEnd <= 0 || indivStepEnd >= indivStepBegin);
            Console.WriteLine("------------------------------------");
            do
                Console.Write("Введите максимальный вес рыбки (double, >0): ");
            while (!double.TryParse(Console.ReadLine(), out maxWeight) || maxWeight <= 0);
            FishUniversal.InitializeData(selectedFunction, dimensionSize, out fishes, populationSize, iterationCount, beginArea, endArea, indivStepBegin, indivStepEnd, maxWeight);
        }
        static void NextIteration()
        {
            if (iterationNumber <= FishUniversal.iterationCount)
            {
                foreach (var fish in fishes)
                    fish.IndividSwim();
                FishUniversal.SearchMaxDeltaFitness(fishes);
                foreach (var fish in fishes)
                    fish.Feeding();
                FishUniversal.SetInstinctAverageWeighted(fishes);
                foreach (var fish in fishes)
                    fish.InstinctSwim();
                FishUniversal.SearchBarycentre(fishes);
                FishUniversal.willStep = FishUniversal.individStep;
                FishUniversal.SumWeightAllFish(fishes);
                foreach (var fish in fishes)
                    fish.CollectiveSwim();
                FishUniversal.PreparingNextIteration(fishes);

            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishLibraryUniversal
{
    public class FishUniversal
    {
        #region Общие данные для всего аквариума
        private static Random randGen = new Random();
        public static double Fitness(params double[] coords)
        {
            return funcTemplate(coords);
        }
        #region Вводимые пользователем данные (с конструктором)

        private static UserFunctions.FuncTemplate funcTemplate;
        /// <summary>
        /// Арность функции (количество аргументов)
        /// </summary>
        private static int dimensionSize;
        /// <summary>
        /// Численность популяции
        /// </summary>
        public static int populationSize;
        /// <summary>
        /// Число итераций (как одно из условий окончания плавания)
        /// </summary>
        public static int iterationCount;
        /// <summary>
        /// Границы аквариума (область исследования)
        /// </summary>
        public static FishPointUniversal lowerBoundPoint, higherBoundPoint;
        //OLD: public static double lowerBoundX, higherBoundX;//Границы аквариума (область исследования)
        /// <summary>
        /// Ограничение на максимальный вес
        /// </summary>
        public static double weightScale;
        /// <summary>
        /// Границы шагов смещения в индивидуальном плавании
        /// </summary>
        public static double individStepStart, individStepFinal;
        public static void InitializeData(UserFunctions.FuncTemplate _function, int _dimensionSize, out FishUniversal[] _fishes, int _popsize, int _iterationCount, FishPointUniversal _lowerBound, FishPointUniversal _higherBound, double _individStepStart = 0.5, double _individStepFinish = 0.1, double _weightScale = 10)
        {
            funcTemplate = _function;
            dimensionSize = _dimensionSize;
            populationSize = _popsize;
            iterationCount = _iterationCount;
            lowerBoundPoint = _lowerBound;
            higherBoundPoint = _higherBound;
            individStepStart = _individStepStart;
            individStepFinal = _individStepFinish;
            individStep = individStepStart;
            weightScale = _weightScale;
            instinctSumWeightOld = populationSize * weightScale / 2;
            _fishes = new FishUniversal[populationSize];
            for (int fishIndex = 0; fishIndex < _fishes.Length; fishIndex++)
            {
                _fishes[fishIndex] = new FishUniversal();
                //Инициализируем положения рыб исходя из арности функции
                for (int swimState = 0; swimState < 4; swimState++)
                    _fishes[fishIndex].swimStatePoint[swimState] = new FishPointUniversal(dimensionSize);
                for (int dimensionIndex = 0; dimensionIndex < dimensionSize; dimensionIndex++)
                    //Выбираем начальное положение для 0-й стадии
                    _fishes[fishIndex].swimStatePoint[0][dimensionIndex] = lowerBoundPoint[dimensionIndex] + (higherBoundPoint[dimensionIndex] - lowerBoundPoint[dimensionIndex]) * randGen.NextDouble();
            }
        }
        #endregion
        #region Общие данные для итерации
        /// <summary>
        /// Значение индивидуального шага смещения на данной итерации
        /// </summary>
        public static double individStep;//(затем содержит шаг предыдущей итерации)
        //Изменяемые в каждой итерации
        /// <summary>
        /// Максимальное изменение фитнесс-функции 
        /// </summary>
        public static double individDeltaFitnessMax;
        /// <summary>
        /// Среднее взвешенное после инстинктивной стадии
        /// </summary>
        public static FishPointUniversal instinctAverage;
        /// <summary>
        /// Вес рыб после инстинктивного плавания в пред. итерации
        /// </summary>
        public static double instinctSumWeightOld;
        /// <summary>
        /// Вес рыб после инстинктивного плавания в текущей итерации
        /// </summary>
        public static double instinctSumWeightNew;
        /// <summary>
        /// Шаг волевого смещения (желательна зависимость от индивид. шага смещения)
        /// </summary>
        public static double willStep;//Шаг волевого смещения
        /// <summary>
        /// Центр тяжести рыб, используется в волевом плавании
        /// </summary>
        public static FishPointUniversal barycentre;
        #endregion
        #endregion
        #region Индивидуальные данные для каждой рыбки
        //Члены объектов
        /// <summary>
        /// 0 - начало индивидуального плавания
        /// 1 -  положение после индвид. плавания, 
        ///2 - после инстинктивного плавания,
        /// 3- окончательная позиция итерации (после коллективного плавания) 
        /// </summary>
        public FishPointUniversal[] swimStatePoint = new FishPointUniversal[4];
        /// <summary>
        /// Смещение после индивидуальной стадии
        /// </summary>
        public FishPointUniversal individDeltaPoint;
        /// <summary>
        /// Изменение функции после индивид. плавания
        /// </summary>
        public double individDeltaFitness;
        public double weight = weightScale / 2;//вес рыбки, по умолчанию половина от максимума
        //Методы рыбок
        /// <summary>
        /// Поиск максимального значения дельта-функции
        /// </summary>
        /// <param name="fishes"></param>
        public static void SearchMaxDeltaFitness(FishUniversal[] fishes)
        {
            individDeltaFitnessMax = fishes.Max(a => a.individDeltaFitness);
        }
        /// <summary>
        /// Подсчет среднего взвешенного индивидуальных движений
        /// </summary>
        /// <param name="fishes"></param>
        public static void SetInstinctAverageWeighted(FishUniversal[] fishes)
        {
            FishPointUniversal numerator = new FishPointUniversal(dimensionSize);
            double denumerator = 0;
            foreach (var fish in fishes)
            {
                numerator += fish.individDeltaPoint * fish.individDeltaFitness;
                denumerator += fish.individDeltaFitness;
            }
            if (denumerator == 0) instinctAverage = new FishPointUniversal(dimensionSize);
            else
                instinctAverage = numerator / denumerator;
        }
        /// <summary>
        /// Поиск и установка центра тяжести системы
        /// </summary>
        /// <param name="fishes"></param>
        public static void SearchBarycentre(FishUniversal[] fishes)
        {
            FishPointUniversal numerator = new FishPointUniversal(dimensionSize);
            double denumerator = 0;
            foreach (var fish in fishes)
            {
                numerator += fish.swimStatePoint[2] * fish.weight;
                denumerator += fish.weight;
            }
            if (denumerator == 0) barycentre = new FishPointUniversal(dimensionSize);
            else
                barycentre = numerator / denumerator;
        }
        /// <summary>
        /// Поиск суммарного веса всех рыб
        /// </summary>
        /// <param name="fishes"></param>
        public static void SumWeightAllFish(FishUniversal[] fishes)
        {
            instinctSumWeightNew = fishes.Sum(fish => fish.weight);
        }
        public static void PreparingNextIteration(FishUniversal[] fishes)
        {
            instinctSumWeightOld = instinctSumWeightNew;
            foreach (var fish in fishes)
            {
                fish.swimStatePoint[0] = fish.swimStatePoint[3];
            }
            //Линейно уменьшаем величину шага индвидуального поиска
            individStep -= (individStepStart - individStepFinal) / iterationCount;
        }
        //Индивидуальные для рыбок методы
        /// <summary>
        /// Кормление рыбки
        /// </summary>
        public void Feeding()
        {
            if (individDeltaFitnessMax != 0)//Избегаем NaN
                this.weight += this.individDeltaFitness / individDeltaFitnessMax;
            if (this.weight > weightScale) this.weight = weightScale;
            if (this.weight < 1) this.weight = 1;
        }
        /// <summary>
        /// Стадия индивидуального плавания 
        /// </summary>
        public void IndividSwim()
        {
            //Даем рыбкам шанс сделать перемещение
            for (int dimensionIndex = 0; dimensionIndex < dimensionSize; dimensionIndex++)
                this.swimStatePoint[1][dimensionIndex] = this.swimStatePoint[0][dimensionIndex] + (-1 + randGen.NextDouble() * 2) * individStep;
            //Delta X и F(x)
            this.individDeltaPoint = this.swimStatePoint[1] - this.swimStatePoint[0];
            this.individDeltaFitness = Fitness(this.swimStatePoint[1].GetCoords()) - Fitness(this.swimStatePoint[0].GetCoords());
            //Проверяем перемещение на эффективность 
            if (!this.swimStatePoint[1].IsInRegion(lowerBoundPoint, higherBoundPoint) | this.individDeltaFitness < 0)
            {
                //Если перемещение неэффективно или невозможно, то полагаем, что его не было
                this.swimStatePoint[1] = this.swimStatePoint[0];
                this.individDeltaPoint = new FishPointUniversal(dimensionSize);
                this.individDeltaFitness = 0;
            }
        }
        /// <summary>
        /// Стадия инстинктивного плавания
        /// </summary>
        public void InstinctSwim()
        {
            this.swimStatePoint[2] = this.swimStatePoint[1] + instinctAverage;
        }
        /// <summary>
        /// Стадия коллективного плавания
        /// </summary>
        public void CollectiveSwim()
        {
            double euclidD = FishPointUniversal.EuclidDistance(this.swimStatePoint[2], barycentre);
            if (euclidD == 0)
                this.swimStatePoint[3] = this.swimStatePoint[2];
            else
                if (instinctSumWeightNew > instinctSumWeightOld)
                    this.swimStatePoint[3] = this.swimStatePoint[2] - (this.swimStatePoint[2] - barycentre) * willStep * randGen.NextDouble() / euclidD;
                else
                    this.swimStatePoint[3] = this.swimStatePoint[2] + (this.swimStatePoint[2] - barycentre) * willStep * randGen.NextDouble() / euclidD;
            //Может и без них сработает
            // if (this.swimStateX[3] > higherBoundX) this.swimStateX[3] = higherBoundX;
            // if (this.swimStateX[3] < lowerBoundX) this.swimStateX[3] = lowerBoundX;
        }
        #endregion
    }
}
